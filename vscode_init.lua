vim.cmd [[nnoremap <silent> <space> <cmd>lua require('vscode-neovim').call('vspacecode.space')<cr>]]

function clear_search_highlight()
    if vim.fn.getreg('/') ~= '' then
        vim.cmd(':let @/ = ""')
    end
end

-- Map <ESC> to clear_search_highlight() and maintain the default behavior of <ESC>
vim.api.nvim_set_keymap('n', '<ESC>', ':lua clear_search_highlight()<CR>', { noremap = true, silent = true })

vim.opt.clipboard = "unnamedplus"
